import { TypeOrmModuleOptions } from "@nestjs/typeorm";
import * as dotenv from 'dotenv'

dotenv.config({path:`./.env.${process.env.NODE_ENV}`})

console.log(process.env.HOST_NAME);
console.log(process.env.DB_PORT);
console.log(process.env.DB_USER);
console.log(process.env.DB_PASSWORD);
console.log(process.env.DB_NAME);
console.log(Boolean(process.env.DB_AUTOLOAD));
console.log(Boolean(process.env.DB_SYNC));

export const typeormconfig:TypeOrmModuleOptions ={
    type: 'mysql',
    host: process.env.HOST_NAME,
    port: parseInt(process.env.DB_PORT),
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    autoLoadEntities: Boolean(process.env.DB_AUTOLOAD) ,
    synchronize: Boolean(process.env.DB_SYNC),
  }

  