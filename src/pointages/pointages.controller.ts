import { Controller, Get, Post, Body, Put, Param, Delete } from '@nestjs/common';
import { PointagesService } from './pointages.service';
import { CreatePointageDto } from './dto/create-pointage.dto';

@Controller('pointages')
export class PointagesController {
  constructor(private readonly pointagesService: PointagesService) {}

  @Post()
  create(@Body() createPointageDto: CreatePointageDto) {
    return this.pointagesService.create(createPointageDto);
  }

  @Get()
  findAll() {
    return this.pointagesService.findAll();
  }
}
