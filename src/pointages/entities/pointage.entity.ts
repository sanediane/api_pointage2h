import { Employe } from "src/employes/entities/employe.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { Horodatage } from './../../utils/horodatage';

@Entity('pointages')
export class Pointage extends Horodatage{
    @PrimaryGeneratedColumn()
    id 

    @Column()
    dateDuJour: string

    @Column()
    heureArrive: string

    @Column()
    heureDepart: string

    @ManyToOne(type=>Employe, (employe)=>employe.pointages)
    employe:Employe
}
