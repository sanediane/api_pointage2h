import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreatePointageDto } from './dto/create-pointage.dto';
import { Pointage } from './entities/pointage.entity';
import { Employe } from 'src/employes/entities/employe.entity';


@Injectable()
export class PointagesService {
  constructor(
    @InjectRepository(Pointage)
      private readonly pointageRepository: Repository<Pointage>,
      @InjectRepository(Employe)
      private readonly employeRepository: Repository<Employe> 
  ){}
  

  async create(createData: CreatePointageDto) {
    const {email_employe} = createData
    const employeExiste = await this.employeRepository.findOne({email_employe})

    if (!employeExiste) {
      throw new NotFoundException(`Cet courriel ${email_employe} n'existe dans la base de données`)
    }
    const employe = employeExiste
    const pointageExiste = await this.pointageRepository.find({employe})

    pointageExiste.forEach(async element => {
      if (element.dateDuJour === createData.dateDuJour) {
        const id = element.id
        const newPointage = await this.pointageRepository.preload({
          id,
          ...createData,
        });

        return await this.pointageRepository.save(newPointage);
      }
      });
    
    
    
    return await this.pointageRepository.save(createData)
  }

  async findAll() {
    return await this.pointageRepository.find()
  }

}
