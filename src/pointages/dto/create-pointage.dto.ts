import { IsEmail, IsNotEmpty, IsOptional, IsString } from "class-validator";


export class CreatePointageDto {
    @IsString()
    @IsNotEmpty()
    @IsEmail()
    email_employe:string

    @IsString()
    @IsNotEmpty()
    dateDuJour:string

    @IsString()
    @IsOptional()
    heureArrive:string

    @IsString()
    @IsOptional()
    heureDepart: string
}
